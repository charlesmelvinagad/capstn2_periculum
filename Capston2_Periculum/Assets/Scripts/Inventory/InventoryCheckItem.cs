﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryCheckItem : MonoBehaviour
{

    public int itemID;
    public int unitNumber;
    public Inventory inv;

    //for puzzle1
    public OpenInventory openInv;
    public GameObject puzzleGear;

    GameObject player;


    //public PuzzleManager puzzleManager;

    public void Start()
    {
        openInv = FindObjectOfType<OpenInventory>();
        inv = GameObject.Find("Inventory").gameObject.GetComponent<Inventory>();
        //puzzleManager = GameObject.Find("Puzzle").GetComponent<PuzzleManager>();
        player = GameObject.Find("Player");
    }

    public void ItemDataPass()
    {
        inv.isChecking = true;
        inv.checkID = itemID;
        inv.invCheckItem = GetComponent<InventoryCheckItem>();
    }

    public void ActivatePuzzle()
    {
        //if (puzzleManager.obj[0] != null && puzzleManager.obj[1] != null && puzzleManager.obj[2] != null && puzzleManager.obj[3] != null)
        //{
        //    puzzleManager.enabled = true;
        //    player.transform.GetChild(0).GetComponent<Camera>().enabled = false;
        //}
    }

    //public void OpenInventory()
    //{
    //    openInv.open = true;
    //    player.GetComponent<PlayerMovement>().enabled = false;
    //    player.GetComponent<CameraRotation>().enabled = false;
    //    player.transform.GetChild(0).GetComponent<CameraRotation>().enabled = false;
    //}

    //public void CloseInventory()
    //{
    //    openInv.open = false;
    //    player.GetComponent<PlayerMovement>().enabled = true;
    //    player.GetComponent<CameraRotation>().enabled = true;
    //    player.transform.GetChild(0).GetComponent<CameraRotation>().enabled = true;
    //}

    //public void InstantiateGear(GameObject item)
    //{
    //    Debug.Log("item exists");
    //    GameObject cubeClone;
    //    Debug.Log(itemID);
    //    Debug.Log("puzzleCube exist");
    //    cubeClone = Instantiate(item, transform.position, transform.rotation) as GameObject;
    //    cubeClone.tag = "Puzzle";
    //    cubeClone.AddComponent<UnitNumber>();
    //    //Destroy(cubeClone.GetComponent<PhysicalItem>());
    //    cubeClone.GetComponent<UnitNumber>().unitNumber = unitNumber;
    //    cubeClone.GetComponent<Rigidbody>().isKinematic = true;
    //    GameObject.Find("Puzzle").gameObject.GetComponent<PuzzleManager>().obj[unitNumber - 1] = cubeClone.gameObject;
    //    cubeClone.transform.parent = GameObject.Find("Puzzle").transform;
    //    Destroy(this.gameObject);
    //}

    //public void InstantiateCode()
    //{

    //}


}
