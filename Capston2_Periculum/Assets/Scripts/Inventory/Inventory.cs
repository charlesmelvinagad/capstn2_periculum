﻿using UnityEngine;
using System.Collections;
using System.Linq;
using UnityEngine.UI;
using System.Collections.Generic;


public class Inventory : MonoBehaviour
{

    public GameObject inventoryPanel;
    public GameObject slotPanel;
    ItemDatabase database;
    public GameObject inventorySlot;
    public GameObject inventoryItem;

    int slotAmount;

    public List<Item> items = new List<Item>();
    public List<GameObject> slots = new List<GameObject>();

    public bool isChecking;
    public int checkID;

    public InventoryCheckItem invCheckItem;

    void Start()
    {
        database = GetComponent<ItemDatabase>();

        inventoryPanel = GameObject.Find("Inventory Panel");
        slotPanel = GameObject.Find("Slot Panel");
       
        InventoryAddSlot();
        AddItem(1);
        //RemoveItem(1);

    }

    public void InventoryAddSlot()
    {
        slotAmount = 0;
        slotAmount++;

        for (int i = 0; i < slotAmount; i++)
        {
            inventorySlot.name = "Slot " + i;
            items.Add(new Item());
            slots.Add(Instantiate(inventorySlot));
            slots[i].GetComponent<Slot>().id = i;
            slots[i].transform.SetParent(slotPanel.transform);
        }
    }

    public void AddItem(int id)
    {
        Item itemToAdd = database.FetchItemByID(id);
        if (itemToAdd.Stackable && CheckIfItemsIsInInventory(itemToAdd))
        {
            for (int i = 0; i < items.Count; i++)
            {
                if (items[i].ID == id)
                {
                    Debug.Log("Add");
                    ItemData data = slots[i].transform.GetChild(0).GetComponent<ItemData>();
                    data.amount++;
                    data.transform.GetChild(0).GetComponent<Text>().text = data.amount.ToString();
                    break;
                }
            }
        }

        else
        {
            for (int i = 0; i < items.Count; i++)
            {
                if (items[i].ID == -1)
                {
                    items[i] = itemToAdd;
                    GameObject itemObj = Instantiate(inventoryItem);

                    itemObj.GetComponent<ItemData>().item = itemToAdd;
                    itemObj.GetComponent<ItemData>().slot = i;
                    itemObj.transform.SetParent(slots[i].transform);
                    itemObj.transform.position = slots[i].transform.position;
                    itemObj.GetComponent<Image>().sprite = itemToAdd.Sprite;
                    itemObj.name = itemToAdd.Title;
                    ItemData data = slots[i].transform.GetChild(0).GetComponent<ItemData>();
                    data.amount = 1;
                    data.transform.GetChild(0).GetComponent<Text>().text = data.amount.ToString();
                    //        invItems.Add(itemObj);

                    if (data.amount == 1)
                    {
                        slots[i].transform.GetChild(0).transform.GetChild(0).GetComponent<Text>().text = "";
                    }

                    break;
                }
            }
        }
    }

    public void RemoveItem(int id)
    {
        Item itemToRemove = database.FetchItemByID(id);

        if (itemToRemove.Stackable && CheckIfItemsIsInInventory(itemToRemove))
        {
            for (int j = 0; j < items.Count; j++)
            {
                if (items[j].ID == id)
                {
                    ItemData data = slots[j].transform.GetChild(0).GetComponent<ItemData>();
                    data.amount--;
                    data.transform.GetChild(0).GetComponent<Text>().text = data.amount.ToString();

                    if (data.amount == 0)
                    {
                        //invItems.Remove(slots[j].transform.GetChild(0).gameObject);
                        Destroy(slots[j].transform.GetChild(0).gameObject);
                        //invItems = invItems.OrderBy(x => x.GetComponent<Inventory>().invItems).ToList();
                        items[j] = new Item();
                        break;
                    }

                    if (data.amount == 1)
                    {
                        slots[j].transform.GetChild(0).transform.GetChild(0).GetComponent<Text>().text = "";
                        break;
                    }

                    break;
                }
            }
        }

        else for (int i = 0; i < items.Count; i++)

                if (items[i].ID != -1 && items[i].ID == id)
                {
                    Destroy(slots[i].transform.GetChild(0).gameObject);
                    items[i] = new Item();
                    break;
                }
    }

    bool CheckIfItemsIsInInventory(Item item)
    {
        for (int i = 0; i < items.Count; i++)
            if (items[i].ID == item.ID)
            {
                return true;
            }
        return false;
    }

}
