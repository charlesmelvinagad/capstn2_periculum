﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class OpenInventory : MonoBehaviour
{

    GameObject player;
    public bool enableAccess;
    public bool open;
    Inventory inv;

    // Use this for initialization
    void Start()
    {

        open = false;
       
        enableAccess = true;
        player = GameObject.Find("Player");
        inv = GameObject.Find("Inventory").GetComponent<Inventory>();

    }

    // Update is called once per frame
    void Update()
    {
        Cursor.visible = open;


        if (enableAccess)
        {
            if (Input.GetKeyDown(KeyCode.Q) || Input.GetKeyDown(KeyCode.Escape))
            {
                inv.isChecking = false;
              
                open = !open;
                Cursor.visible = open;

                //DisableCamera(open);
            }
        }


        for (int i = 0; i < inv.slots.Count; i++)
        {
            inv.inventoryPanel.GetComponentInParent<Canvas>().enabled = open;
            inv.inventoryPanel.GetComponent<Image>().enabled = open;
            inv.inventorySlot.GetComponent<Image>().enabled = open;
            inv.slots[i].GetComponent<Image>().enabled = open;
        }
    }

    //void DisableCamera(bool active)
    //{
    //    player.GetComponent<CameraRotation>().enabled = !active;
    //    player.GetComponent<PlayerMovement>().enabled = !active;
    //    player.transform.GetChild(0).GetComponent<CameraRotation>().enabled = !active;
    //}

}
