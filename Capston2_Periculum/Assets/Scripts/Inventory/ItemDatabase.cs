﻿using UnityEngine;
using System.Collections;
using LitJson;
using System.Collections.Generic;
using System.IO;



public class ItemDatabase : MonoBehaviour
{
    public List<Item> database = new List<Item>();
    private JsonData itemData;


    void Start()
    {
        itemData = JsonMapper.ToObject(File.ReadAllText(Application.dataPath + "/StreamingAssets/Items.json"));
        ConstructItemDatabase();

    }

    public Item FetchItemByID(int id)
    {
        for (int i = 0; i < database.Count; i++)
            if (database[i].ID == id)
                return database[i];
        return null;
    }



    void ConstructItemDatabase()
    {
        for (int i = 0; i < itemData.Count; i++)
        {
            database.Add(new Item((int)itemData[i]["id"], itemData[i]["title"].ToString(), (int)itemData[i]["value"],
            itemData[i]["description"].ToString(), (bool)itemData[i]["stackable"], (bool)itemData[i]["craftable"],
            itemData[i]["slug"].ToString()));
        }
    }


}

[System.Serializable]
public class Item
{
    public int ID { get; set; }
    public string Title { get; set; }
    public int Value { get; set; }
    public string Description { get; set; }
    public bool Stackable { get; set; }
    public bool Craftable { get; set; }
    public string Slug { get; set; }
    public Sprite Sprite { get; set; }
    public GameObject GameObject { get; set; }

    public Item(int id, string title, int value, string description, bool stackable, bool craftable, string slug)
    {
        this.ID = id;
        this.Title = title;
        this.Value = value;
        this.Description = description;
        this.Slug = slug;
        this.Stackable = stackable;
        this.Craftable = craftable;
        this.Sprite = Resources.Load<Sprite>("Sprites/Items/" + slug);
        this.GameObject = Resources.Load<GameObject>("GameObject/Items/" + slug);
    }


    public Item()
    {
        this.ID = -1;
    }
}

