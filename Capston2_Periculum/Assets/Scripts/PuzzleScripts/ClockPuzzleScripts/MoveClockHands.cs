﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveClockHands : MonoBehaviour {

    public ClockData cData;
    public Transform minutes, hours;

	void Update () {

        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            minutes.Rotate(Vector3.forward * cData.minuteSpeed);
            HourHandMove(0, 0, cData.hourSpeed);
        }

        else if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            minutes.Rotate(Vector3.forward * -cData.minuteSpeed);
            HourHandMove(0, 0, -cData.hourSpeed);
        }

    }

    public void HourHandMove(int x, int y, float z)
    {
        hours.Rotate(x, y, z * 1);

    }

   
}
