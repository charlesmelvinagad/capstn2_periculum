﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryDisplay : MonoBehaviour {

    public Canvas inventoryCanvas;
    public CursorDisplay cursorDisplay;
    public  bool inventoryOpen;

	void Start () {

        inventoryCanvas.rootCanvas.enabled = inventoryOpen;
        inventoryOpen = false;
       
	}
	

	void Update () {

      

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            cursorDisplay.enableCursor = !cursorDisplay.enableCursor;
            inventoryCanvas.enabled = !inventoryCanvas.enabled;
        }

	}
}
