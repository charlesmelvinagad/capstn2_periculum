﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class TagsClass
{
    public string m_InteractTag = "Interact";
    public string m_InteractItemsTag = "InteractItem";
    public string m_DoorsTag = "Door";
}

[System.Serializable]
public class DoorGrabClass
{
    public float m_DoorPickupRange = 2f;
    public float m_DoorThrow = 10f;
    public float m_DoorDistance = 2f;
    public float m_DoorMaxGrab = 3f;

}

public class Interaction : MonoBehaviour
{
    public TagsClass Tags = new TagsClass();
    public DoorGrabClass DoorGrab = new DoorGrabClass();

    protected bool isObjectHeld;
    protected bool tryPickupObject;

    private float PickupRange = 3f;
    private float ThrowStrength = 50f;
    private float distance = 3f;
    private float maxDistanceGrab = 4f;

    public GameObject objectHeld;

    public bool isTalking;

    void Start()
    {
        isObjectHeld = false;
        tryPickupObject = false;
        objectHeld = null;
    }

    //public Camera MainCamera()
    //{
    //    return Camera.main;
    //}

    public void InteractItems(RaycastHit _hit)
    {
        if (_hit.collider.tag == "InteractItem")
        {
            _hit.collider.GetComponent<GlowObject>().GlowOn();
            return;
        }
    }

    public void Subtitle(RaycastHit _hit)
    {
        isTalking = true;
        if (_hit.collider.GetComponent<TextAssetHolder>() != null)
        _hit.collider.GetComponent<TextAssetHolder>().SendText();
        return;
    }

    public void DoorInteract(RaycastHit _hit)
    {
        if (_hit.collider.tag == Tags.m_DoorsTag && tryPickupObject)
        {
            objectHeld = _hit.collider.gameObject;
            isObjectHeld = true;

            objectHeld.GetComponent<Rigidbody>().useGravity = true;
            objectHeld.GetComponent<Rigidbody>().freezeRotation = false;

            PickupRange = DoorGrab.m_DoorPickupRange;
            ThrowStrength = DoorGrab.m_DoorThrow;
            distance = DoorGrab.m_DoorDistance;
            maxDistanceGrab = DoorGrab.m_DoorMaxGrab;
        }
    }

    public void holdObject(Ray _ray, Camera _cam)
    {
        //Ray _ray = playerCam.GetComponent<Camera>().ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));

        Vector3 nextPos = Camera.main.transform.position + _ray.direction * distance;
        Vector3 currPos = objectHeld.transform.position;

        objectHeld.GetComponent<Rigidbody>().velocity = (nextPos - currPos) * 10;

        if (Vector3.Distance(objectHeld.transform.position, _cam.transform.position) > maxDistanceGrab)
        {
            DropObject();
        }
    }

    public void DropObject()
    {
        isObjectHeld = false;
        tryPickupObject = false;
        objectHeld.GetComponent<Rigidbody>().useGravity = true;
        objectHeld.GetComponent<Rigidbody>().freezeRotation = false;
        objectHeld = null;
    }

  

}
