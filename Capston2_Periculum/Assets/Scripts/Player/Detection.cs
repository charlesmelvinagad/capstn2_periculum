﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Detection : Interaction {

    //public CameraController cam;
    public CursorDisplay cursorDisplay;
  //  public Text nameText;
   // public Text instruction;
    public int rayLength;

    RaycastHit hit;

    public KeyCode InputKeyE()
    {
        return KeyCode.E;
    }

    public Camera MainCamera()
    {
        return Camera.main;
    }

    public Ray GetRay()
    {
        return Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
    }

    // Update is called once per frame
    void Update () {

   
        RayCast(GetRay());
    }

    public void RayCast(Ray _ray)
    {
        if (Physics.Raycast(_ray, out hit, rayLength))
        {
            InteractExecute();
            InteractItems(hit);
            KeyCodeE(hit);
        }
    }

    public void InteractExecute()
    {
        if (Input.GetMouseButton(0))
        {
            DoorInteract(hit);


            if (!base.isObjectHeld)
            {
                // Debug.Log(hit.transform.name);

                tryPickupObject = true;
            }

            else if (base.isObjectHeld)
            {
                holdObject(GetRay(), MainCamera());
            }
        }

        else if (base.isObjectHeld)
        {
            DropObject();
        }
    }

    public void KeyCodeE(RaycastHit _hit)
    {
        if (_hit.collider.tag == "InteractItem")
        {
            if (Input.GetKey(InputKeyE()))
            {
                if (!isTalking)
                {
                    Subtitle(hit);
                }
            }
        }
    }

}
