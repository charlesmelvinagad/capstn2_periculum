﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class SRTHandler : MonoBehaviour 
{
	public static SRTHandler Instance;

	private SRTReader reader;

	public List<SRT> activeSRTList;
	public int currentIndexOfSubtitle = -1;
	public TextAsset srtFile;
    public Detection detection;

    void Awake()
	{
		Instance = this;
	}

	void Start()
	{
        detection = FindObjectOfType<Detection>();
    }

    public void LoadSRT(string asset)
    {
        reader = new SRTReader();

        if (reader.Load(Application.dataPath + "/StreamingAssets/" + asset + ".txt"))
        {
            activeSRTList = reader.getSRTList();
            StartCoroutine(startSrt(activeSRTList));
        }

        else
            Debug.LogError("Can't load file");
    }

    public void StopSRT()
    { 
      StopCoroutine(startSrt(activeSRTList));
    }

	IEnumerator startSrt(List<SRT> array)
	{
		foreach (var item in array) {
			currentIndexOfSubtitle = array.IndexOf(item);
			TextView.Instance.setText(item.text);
			yield return new WaitForSeconds(item.time);
		}
		currentIndexOfSubtitle = -1;
		TextView.Instance.setText("");

        detection.isTalking = false;

        this.gameObject.GetComponent<SRTHandler>().enabled = false;

      
	}


	IEnumerator startSrtFromPointToPoint(List<SRT> array , int startPoint , int finalPoint)
	{
		if (finalPoint > array.Count) {
			Debug.LogError("Final point is biger than array lenght");
			yield return false;
		}
		if (startPoint > finalPoint) {
			Debug.LogError("Final point is less than array startPoint");
		    yield return false;
		}
		for (int i = startPoint; i <= finalPoint; i++) {
			currentIndexOfSubtitle = i;
			TextView.Instance.setText(array[i].text);
			yield return new WaitForSeconds(array[i].time);
		}
		currentIndexOfSubtitle = -1;
		TextView.Instance.setText("Finished");
	}
	



}
