﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextAssetHolder : MonoBehaviour {

    //public TextAsset srtFile;
    public string txtString;
    public SRTHandler srt;

    // Use this for initialization
    void Start () {

        srt = GameObject.FindObjectOfType<SRTHandler>();

        //if (srtFile != null)
       // txtString = srtFile.name;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SendText()
    {

        srt.enabled = true;
        srt.GetComponent<TextView>().text.enabled = true;
        srt.GetComponent<SRTHandler>().srtFile = null;
        srt.GetComponent<SRTHandler>().LoadSRT(txtString);
    }

    public void CancelText()
    {
        srt.GetComponent<Text>().enabled = false;
        srt.GetComponent<SRTHandler>().srtFile = null;
        srt.enabled = false;
        srt.GetComponent<SRTHandler>().StopSRT();
    }

}
