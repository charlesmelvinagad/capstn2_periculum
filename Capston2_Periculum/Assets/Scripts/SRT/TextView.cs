﻿    using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Text;
using System.IO;
using System.Collections.Generic;

public class TextView : MonoBehaviour
{
    public static TextView Instance;
    public bool isShowingText;
    public Text text;

    void Awake()
    {
        Instance = this;
        isShowingText = false;
     
    }

    public void setText(string _text)
    {
        text.text = _text;
    }

}
