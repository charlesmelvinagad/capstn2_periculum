﻿using UnityEngine;
using UnityEditor;

public class ObjectNumberDataAsset
{
    [MenuItem("Assets/Create/ObjectNumberDataAsset")]
    public static void CreateAsset()
    {
        ScriptableObjectUtility.CreateAsset<ObjectNumberData>();
    }
}
