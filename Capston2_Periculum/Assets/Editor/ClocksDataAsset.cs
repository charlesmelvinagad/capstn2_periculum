﻿using UnityEngine;
using UnityEditor;

public class ClockDataAsset
{
    [MenuItem("Assets/Create/ClockDataAsset")]
    public static void CreateAsset()
    {
        ScriptableObjectUtility.CreateAsset<ClockData>();
    }
}
